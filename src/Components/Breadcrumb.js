import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Breadcrumb as BreadcrumbAntd } from 'antd';

const Breadcrumb = (props) => {
    const breadcrumb = useSelector(state => state.breadcrumb)
    const [pagePath, setPagePath] = useState(breadcrumb)
    useEffect(() => {
        setPagePath(breadcrumb)
    }, [breadcrumb])

    return(
        <BreadcrumbAntd items={!pagePath || pagePath.length < 2 ? [{title : 'Home'}] : pagePath}/>
    );
}
export default Breadcrumb;