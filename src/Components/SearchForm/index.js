import { Button, Col, DatePicker, Form, Input, Row, Select, Space, Switch } from 'antd';
import React, { useState } from "react";
import { useEffect } from 'react';
const {Search} = Input;
const SearchForm = (props) => {
    let {
        layout,
        listColumn,
        onSubmit
    } = props;
    const [Items, setItems] = useState([]);
    const [form] = Form.useForm();
    const [IsShow, SetIsShow] = useState(false);

    useEffect(() => {
        const items = elementSearch();
        setItems(items);
        SetIsShow(true);
    }, [listColumn]);

    const bindData = (param) => {
        let data = [];
        if(!!param.url)
        {
            //code call data from api
            data = [
                {
                    label : 'this is lable from api',
                    value : 'api'
                }
            ]
        }
        else
        {
            // code call cache here
            data = [
                {
                    label : 'this is lable from cache',
                    value : 'cache'
                }
            ]
        }

        return data;
    }

    const elementSearch = () => {
        const children = listColumn?.map((item, index) => {
            switch(item.type)
            {
                case 'select': 
                    let listOption = !!item.isLoadCache && item.isLoadCache? bindData(item) : !!item.listOption? item.listOption : [];
                    let options = listOption.map((option, indexOption) => {
                        return <Select.Option value={option.value} key={indexOption + 2}>{option.label}</Select.Option>
                    });
                    return <Col xs={24} sm={4} span={4} key={index}>
                                <Form.Item name={item.name} label={item.label} rules={item.rules} initialValue='-1'>
                                    <Select>
                                        <Select.Option value='-1' key={1}>Vui lòng chọn</Select.Option>
                                        {
                                            options
                                        }
                                    </Select>
                                </Form.Item>
                            </Col>
                case 'textbox' : 
                    return <Col xs={24} sm={4} span={4} key={index}>
                                <Form.Item name={item.name} label={item.label} rules={item.rules}>
                                    <Input placeholder={item.placeholder}/>
                                </Form.Item>
                            </Col>
                case 'date' : 
                return <Col key={index}>
                            <Form.Item name={item.name} label={item.label} rules={item.rules}>
                                <DatePicker placement={!!item.placement ? item.placement : 'topLeft'} />
                            </Form.Item>
                        </Col>
                default : 
                    return <>
                    </>
            }
        });
        
        return children;
    }

    const onFinish = (values) => {
        onSubmit?.(values);
    };

    return(
        IsShow?
        <Form form={form} layout={layout} name='search_form' onFinish={onFinish}>
            {
                window.innerWidth >= 500?
                <Row gutter={24}>
                    {Items}
                    <Form.Item style={{marginTop: layout == 'vertical'? 30 : 0}}><Button type="primary" htmlType="submit">Tìm</Button></Form.Item>
                </Row>
                :
                <>
                    {Items}
                    <Col span={4}>
                        <Form.Item style={{float : 'left'}}><Button type="primary" htmlType="submit">Tìm</Button></Form.Item>
                    </Col>
                </>
            }
        </Form>:<></>
    );
}
export default SearchForm;