import React, { useState } from 'react';
import { Layout, theme } from 'antd';
import Menu from './Menu';
import MyHeader from './MyHeader';
import Breadcrumb from './Breadcrumb'

const { Content, Footer } = Layout;

const Layouts = (props) => {
    const [collapsed, setCollapsed] = useState(false);
    const { token: { colorBgContainer }, } = theme.useToken();
    if (props.layout === 'pagenotfound') {
        return (
            props.children
        );
    }
    else {
        return (
            <Layout>
                <Menu collapsed={collapsed}/>
                <Layout>
                    <MyHeader setCollapsed={setCollapsed} collapsed={collapsed}/>
                    <Content
                        style={{
                            margin: '10px 10px 0',
                            overflow: 'initial',
                            minHeight: `calc(100vh - 145px)`,
                        }}
                    > 
                        <div style={
                            {
                                padding : '5px 0 8px 24px',
                                overflow: 'initial',
                                margin: '0 0 8px',
                                background: colorBgContainer,
                                borderRadius : 7,
                                fontWeight : 500
                            }
                        }>
                            <Breadcrumb />
                        </div>
                        <div style={{
                            padding: 24,
                            textAlign: 'center',
                            background: colorBgContainer,
                            borderRadius : 7
                        }}>
                            {
                                props.children
                            }
                        </div>
                    </Content>

                    <Footer style={{ textAlign: 'center'}}> Học làm web với Antd ©2023 </Footer>
                </Layout>
            </Layout>
        );
    }
}

export default Layouts;


