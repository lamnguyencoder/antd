import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { setBreadcrumb } from './redux/reducers';
import { _fetchData } from '../utils/callAPI';
import { HOSTNAME } from '../utils/constants/systemVars';
import { Notification } from '../utils/notification';

const DashBoard = (props) => {
    const PagePath = [{ href: "/", title: 'Home'}];
    const dispatch = useDispatch();
    const [datasource, setDatasource] = useState([]);

    useEffect(() => {
        dispatch(setBreadcrumb(PagePath));
        loadData();
        return () => {}
    },[]);

    const loadData = async () => {
        const response = await dispatch(_fetchData(HOSTNAME, 'api/user/search', {}));
        if(!response.iserror)
        {
            setDatasource(response?.resultObject);
        }
        else
        {
            Notification('Thông báo', response.message, 'error');
        }
    }

    return (
        <>
            <h3>Đây là trang home</h3>
        </>
    );
}
export default DashBoard;