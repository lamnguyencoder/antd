import { combineReducers } from 'redux'
import breadcrumbReducer from './breadcrumbReducer'
import userReducer from './userReducer'
import loaddingReducer from './loaddingReducer'
export * from './breadcrumbReducer'
export * from './userReducer'
export * from './loaddingReducer'

export const rootReducer = combineReducers({
  breadcrumb: breadcrumbReducer,
  user: userReducer,
  loadding: loaddingReducer,
})