import React from 'react';
import {
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    UserOutlined
} from '@ant-design/icons';

import { Layout, Button, theme, Avatar, Row, Col, Dropdown, Menu } from 'antd';
import { useNavigate } from 'react-router-dom';
const Screen = window.innerWidth;
const { Header } = Layout;
const MyHeader = (props) => {
    const userInfo = JSON.parse(localStorage.getItem('logininfo'));
    const navigate = useNavigate();
    const { token: { colorBgContainer }, } = theme.useToken();
    const items = [
        {
          key: '1',
          label: 'Thông tin nhân viên'
        },
        {
          key: '2',
          label: 'Đổi mật khẩu'
        },
        {
            key: '3',
            label: <span onClick={() => Logout()}>Đăng xuất</span>
        }
      ];

    const Logout = () => {
        localStorage.removeItem('logininfo');
        navigate('/login');
    }
    return(
        <Header
            style={{
                padding: 0,
                background: colorBgContainer,
            }}
            >
                <Row>
                    <Col flex="auto">
                        {
                            window.innerWidth > 290?
                                <Button
                                    type="text"
                                    icon={props.collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
                                    onClick={() => props.setCollapsed?.(!props.collapsed)}
                                    style={{
                                    fontSize: '16px',
                                    width: 64,
                                    height: 64
                                }}/> : ""
                        }
                        
                    </Col>
                    <Col flex="auto">
                        <Dropdown menu={{items,}} placement="bottomLeft"arrow >
                            <div style={{
                                    whiteSpace : 'nowrap',
                                    overflow : 'hidden',
                                    textOverflow : 'ellipsis',
                                    float : 'right',
                                    marginRight : '10px'
                                }}>
                                <Avatar size='default' icon={<UserOutlined />}></Avatar>
                                {
                                    !props.collapsed && Screen < 400 ? "" : Screen >= 900 ? ` ${userInfo.username} - Lâm Xuân Nguyên` : Screen >= 400 && props.collapsed ? ` ${userInfo.username} - Lâm Xuân Nguyên` : ` ${userInfo.username}`
                                }
                            </div>
                        </Dropdown> 
                    </Col>
                </Row>
        </Header>
    );
}
export default MyHeader;