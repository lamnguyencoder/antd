import React from 'react';
import {Link, useLocation} from 'react-router-dom'
import { Layout, Menu as MenuAntd} from 'antd';
import AppMenu from '../views';
import { DashOutlined } from '@ant-design/icons';
const { Sider } = Layout;

const Menu = (props) => {
    let {
        collapsed
    } = props;
    const location = useLocation();
    const keys = location.pathname.split('/');
    const getItem = (label = null, key = null, icon = null, children = null, path = null, type= null) => {
        let subMenu, item;
        if(!!children && children.length > 0)
        {
            subMenu = children.map(sub => {
                return getItem(sub.label, sub.name, sub.icon, sub.subItem, sub.path);
            });
        }
        // else
        // {
        //     subMenu = children;
        // }
        if(!!subMenu)
        {
            item = {
                label, 
                key, 
                icon : !!icon? React.createElement(icon) : <DashOutlined />, 
                children : subMenu , 
                type
            }
        }
        else
        {
            item = {
                label : <Link to={path}> {label} </Link>, 
                key, 
                icon : !!icon? React.createElement(icon) : <DashOutlined />, 
                children : subMenu,
                type
            }
        }

        return item;
    }

    const items = AppMenu.map((item) => {
        return getItem(item.label, item.name, item.icon, item.subItem, item.path)
    })

    return(
        <Sider trigger={null} collapsible collapsed={window.innerWidth < 290 ? true : collapsed}>
            <div className="demo-logo-vertical" />
            <MenuAntd
                theme="dark"
                mode="inline"
                items={items}
            />
        </Sider>
    );
}
export default Menu;