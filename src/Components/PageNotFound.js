import React from 'react';
import { Button, Result } from 'antd';
import { useNavigate } from 'react-router-dom';
const PageNotFound = (props) => {
    let navigate = useNavigate();
    const btnClick = () => {
        navigate('/',{})
    }
    return (
        <Result
            status="404"
            title="404"
            subTitle="Rât tiếc, trang bạn truy cập không tồn tại."
            extra={<Button type="primary" onClick={btnClick}>Trang chủ</Button>}
        />
    );
}
export default PageNotFound;