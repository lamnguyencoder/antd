import { Button, Col, DatePicker, Divider, Form, Input, InputNumber, Row, Select, Slider, Space, Switch, Upload } from "antd";
import React from "react";
import {
    PlusOutlined
} from '@ant-design/icons';
import { useEffect } from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
const { TextArea } = Input;
const FormContainer = (props) => {
    let {
        title,
        listColumn,
        layout,
        onCloseModal,
        backLink,
        onSubmit
    } = props;
    const navigate = useNavigate();
    const [Items, setItems] = useState([]);
    useEffect(() => {
        const items = renderControl();
        setItems(items);
    }, [listColumn]);

    const renderControl = () => {
        const children = listColumn?.map((elm, index) => {
            switch(elm.type)
            {
                case 'textbox': 
                    return <Col span={!!elm.span? elm.span : 12} key={index}>
                            <Form.Item label={elm.label} name={elm.key}>
                                <Input />
                        </Form.Item>
                    </Col>
                case 'select': 
                return <Col span={!!elm.span? elm.span : 12} key={index}>
                        <Form.Item label={elm.label} name={elm.key}>
                            <Select>
                                <Select.Option value="demo">Demo</Select.Option>
                        </Select>
                    </Form.Item>
                </Col>
                case 'textarea': 
                return <Col span={!!elm.span? elm.span : 24} key={index}> 
                        <Form.Item label={elm.label} name={elm.key}>
                            <TextArea rows={!!elm.row? elm.row : 4} />
                    </Form.Item>
                </Col>
                default : return <></>;
            }
        });
        return children;
    }
    const eventBackLink = () =>{
        if(!!onCloseModal)
        {
            onCloseModal?.();
        }
        else
        {
            navigate(backLink);
        }
    }

    const onFinish = (values) => {
        onSubmit?.(values);
    }
    return <>
        {!!title? <Divider>{title}</Divider> : ""}
        <Form layout={!!layout ? layout : 'horizontal'} onFinish={onFinish} name='s_form'>
            <Row gutter={24}>
                {
                    Items
                }
            </Row>
            <div
                style={{
                textAlign: 'right',
                }}
            >
                <Space size="small">
                    <Button type="primary" htmlType="submit">
                        Cập nhật
                    </Button>
                    <Button
                        onClick={() => {eventBackLink()}}
                    >
                        Quay lại
                    </Button>
                </Space>
            </div>
        </Form>
        {
            !!props.children ? 
            <>
                <Divider></Divider>
                <div>
                    {
                        props.children
                    }
                </div>
            </> 
            : 
            <>
            </>
        }
        
    </>
}
export default FormContainer;