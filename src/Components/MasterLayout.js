import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import Config from '../routers/Config'
import Layouts from './Layouts';
const MasterLayout = (props) => {
    return (
        <React.Suspense fallback={<div>Loading... </div>}>
            <Routes>
                {
                    Config.map(({ component: Component, path, layout, breadcrumb }) => (
                        <Route path={path} key={path} element={Component ? <Layouts layout={!!layout ? layout : 'Nguin'}><Component /></Layouts> : <Navigate to={'/'} replace={true} />} />
                    ))
                }
            </Routes> 
        </React.Suspense>
    );
}
export default MasterLayout;