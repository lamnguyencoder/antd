import { Checkbox, Dropdown, Space, Switch, Tag, Tooltip } from "antd";
import { CloseCircleOutlined, SyncOutlined, CheckCircleTwoTone ,DeleteFilled, DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Link } from "react-router-dom";
import dayjs from 'dayjs'
export const PagePath = [{ href: "/", title: "Home"},{ title: "List of user", }];

export const AddPagePath = [{ href: "/", title: "Home"},{ href: "/User", title: "List of user" },{ title: "Add"}];

export const SearchElement = [
    // {
    //     type: 'search',
    //     label : 'Tìm kiếm',
    //     name : 'KeyWord',
    //     rules : [{
    //         require : true, message : 'Không được bỏ trống!'
    //     }],
    //     placeholder : 'Nhập từ khóa tìm kiếm'
    // },
    {
        type: 'textbox',
        label : 'Text',
        name : 'textbox',
        // rules : [{
        //     required : true, message : 'Không được bỏ trống!'
        // }],
        placeholder : 'Nhập từ khóa tìm kiếm'
    },
    {
        type: 'select',
        label : 'Loại',
        name : 'selectbox',
        rules : [{
            required : true, message : 'Vui lòng chọn!'
        }],
        // listOption : [
        //     {
        //         label : 'Loại số 1',
        //         value : '1'
        //     }
        // ],
        isLoadCache : true,
        url : "api/xxxxx/xxxxx"
    },
    {
        type : 'date',
        label : 'Từ ngày',
        name : 'fromDate'
    },
    {
        type : 'date',
        label : 'Đến ngày',
        name : 'toDate'
    }
    
];

export const  columns = [
  {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      render: (key, item) => (
          <Link key={key} to={`/detail/${item.id}`}>{`${item.id} - ${item.name}`}</Link>
      ),
      fixed: 'left', // cố định cột 
      width: 150,

  },

  {
      title: 'Action',
      key: 'groupAction',
      dataIndex: 'groupAction',
      width: 100,
      align: 'center',
      link: '/edit/',
  },
  {
      title: 'Action update',
      key: 'edit',
      dataIndex: 'edit',
      align: 'center',
      width: 100,
      link: '/edit/',
      icon: <EditOutlined />

  },
  {
      title: 'Action delete',
      key: 'delete',
      dataIndex: 'delete',
      align: 'center',
      width: 100,
      icon: <DeleteOutlined />
  },
  {
      type: 'number',
      title: 'Age',
      dataIndex: 'age',
      key: 'age',
      width: 100,
      sorter: (a, b) => a.age - b.age, // sắp xếp

  },
  
//   {
//       type: 'text',
//       title: 'Address',
//       dataIndex: 'address',
//       key: 'address',
//       width: 500,
//       ellipsis: {// Nội dung quá dài 1 ô thì ...
//           showTitle: false,
//       },
//       render: (address) => (
//           <Tooltip placement="topLeft" title={address}>
//               {address}
//           </Tooltip>
//       ),

//   },
//   {
//       title: 'Status',
//       dataIndex: 'statusID',
//       key: 'statusID',
//       width: 300,
//       render: (statusID, item, key) => (
//           // console.log("statusID", statusID, item, key)
//           <Space size={[0, 8]} key="key">
//               <Tag icon={<CloseCircleOutlined />} bordered={false} color="error">Reject</Tag>
//               <Tag icon={<SyncOutlined spin />} bordered={false} color="processing">In progress</Tag>
//               <Tag icon={<CheckCircleTwoTone twoToneColor="#52c41a" />} color="#87d068">Finish</Tag>
//           </Space>
//       ),
//   },
//   {
//       title: 'Actived',
//       dataIndex: 'isActived',
//       key: 'isActived',
//       width: 100,
//       render: (isActived, item) => (
//           <Switch size="default" key={item.key} disabled={true} defaultChecked={isActived} checked={isActived} />
//       ),
//   },
//   {
//       title: 'System',
//       dataIndex: 'isSystem',
//       key: 'isSystem',
//       width: 100,
//       render: (isSystem, item) => (
//           <Checkbox size="default" value={isSystem} disabled={true} key={item.key} defaultChecked={isSystem} checked={isSystem} />
//       ),
//   },
//   {
//       title: 'Update Date',
//       dataIndex: 'updateDate',
//       key: 'date',
//       width: 150,
//       render: (cellSelect, item, key) => (
//           <label key={key}>{dayjs(cellSelect).format('DD-MM-YYYY HH:mm')}</label>
//       ),

//   },
//   {
//       title: 'Update Full Name',
//       dataIndex: 'updateUser',
//       key: 'updateUser',
//       width: 300,
//       render: (cellSelect, item, key) => (
//           <div key={key} >{`${item.updateUser} - ${item.updateFullName}`}</div>
//       ),
//   },



];

export const listColumnModel = [
    {
        type : 'textbox',
        key : 'UserID',
        label : 'Mã nhân viên'
    },
    {
        type : 'textbox',
        key : 'UserName',
        label : 'Tên nhân viên'
    }
]