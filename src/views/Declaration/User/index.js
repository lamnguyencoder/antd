import React from 'react';
const Search = React.lazy(() => import('./Search'));
const Add = React.lazy(() => import('./Add'));

const User = [
    {
        path : '/User',
        component : Search
    },
    {
        path : '/User/Add',
        component : Add
    },
]
export default User;