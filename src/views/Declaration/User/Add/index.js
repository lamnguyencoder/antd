import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { setBreadcrumb } from '../../../../Components/redux/reducers';
import { AddPagePath } from '../constants';
import FormContainer from '../../../../Components/FormContainer';
import { Input } from 'antd';

const Add = (props) => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(setBreadcrumb(AddPagePath));
    }, []);
    const listColumn = [
        {
            type : 'textbox',
            key : 'UserID',
            label : 'Mã nhân viên'
        },
        {
            type : 'textbox',
            key : 'UserName',
            label : 'Tên nhân viên'
        }
    ]
    return <>
        <FormContainer 
            title='Thêm mới nhân viên'
            listColumn={listColumn}
            backLink='/User'
        >
            <Input placeholder='...aaaaaaaadasdasdasdadsafsdfsdgfdfgdgdfghfhfhfhgugjgjtgj'/>
        </FormContainer>
    </>
}
export default Add;