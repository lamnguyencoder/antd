import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { setBreadcrumb } from '../../../../Components/redux/reducers';
import { PagePath, SearchElement, columns, listColumnModel } from '../constants';
import DataGird from '../../../../Components/DataGird';
import { DEFAULT_TABLE_PAGE_SIZE_NUMBER, DEFAULT_TABLE_PAGE_SIZE_OPTIONS } from '../../../../Components/constants/tableConfig';
import SearchForm from '../../../../Components/SearchForm';
import { Spin, message } from 'antd';
const Search = (props) => {
    const dispatch = useDispatch();
    const [data, setData] = useState([]);
    const [isLoadComplete, setisLoadComplete] = useState(false);
    useEffect(() => {
        dispatch(setBreadcrumb(PagePath));
        prePareData();
    }, []);

    const handleSelectRow = (rowKeys, selectedRows) => {
        console.log("rowKeys, selectedRows", rowKeys, selectedRows)
    }

    const handleChangePage = (pageNumber) => {
        console.log("handleChangePage", pageNumber)
    }

    const handleDeleteRow = (rows) => {
        console.log("handleDeleteRow", rows)

    }

    const prePareData = () => {
        setisLoadComplete(false);
        let dataSource = [];
        for (let i = 0; i < 2; i++) {
            dataSource.push({
                key: i,
                id: `DT${i}`,
                name: `Edward ${i}`,
                isActived: i % 2 == 0 ? true : false,
                isSystem: true,
                statusID: 1,
                age: 32 + i,
                address: ` ${i} New York No. ${i} Lake Park, New York No. ${i} Lake Park`,
                updateDate: new Date(),
                updateUser: "administrator",
                updateFullName: "QT hệ thống",
            });
        }
        setData(dataSource);
        setisLoadComplete(true)
    }
    const onSubmit = (MLObject) => {
        console.log('MLObject', MLObject);
        message.success('Submit success!');
        // message.error('Submit failed!');
    }
    const onSubmitModel = (MLObject) => {
        console.log('MLObject', MLObject);
    }
    
    return (
        isLoadComplete?
        <>
        <SearchForm 
            listColumn={SearchElement} 
            layout='vertical'
            onSubmit={onSubmit}
        />
        <DataGird
                title = 'Danh sách nhân viên'
                listColumn={columns}
                dataSource={data}
                isShowHeaderAction={true}
                isShowButtonAdd={true}
                urlAdd='/User/Add'
                isShowModalBtnAdd={true}
                listColumnModel={listColumnModel}
                onSubmitModel={onSubmitModel}
                isShowHeaderTable={true}
                pageSizeNumber={DEFAULT_TABLE_PAGE_SIZE_NUMBER}
                onSelectRowItem={handleSelectRow}
                onDeleteItem={handleDeleteRow}
                onPageChange={handleChangePage}
                isShowSizeChanger={false}
                pageSizeOptions={DEFAULT_TABLE_PAGE_SIZE_OPTIONS}
            /></> : <label><Spin tip="Đang tải dữ liệu" size="small"><div className="content" /></Spin></label>
    );
}
export default Search;