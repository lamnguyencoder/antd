import React from 'react';
const Search = React.lazy(() => import('./Search'));
const Product = [
    {
        path : '/Product',
        component : Search
    },
]
export default Product;