import Product from "./Product";
import User from "./User";
export const routes = [
    ...User,
    ...Product
];