import './App.css';
import React from 'react'
import Router from './routers';

const App = (props) => {
  return (
    <React.Suspense fallback={<div>Loading...</div>}>
        <Router />
    </React.Suspense>
  );
}
export default App;
