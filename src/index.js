import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from './Components/redux/store';

const root = ReactDOM.createRoot(document.getElementById('app'));
root.render(
  <Provider store={store}>
    {/* <React.StrictMode> */}
      <BrowserRouter>
          <App />
      </BrowserRouter>
    {/* </React.StrictMode> */}
  </Provider>
);


reportWebVitals();
