import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Login from '../Components/Login';
import MasterLayout from '../Components/MasterLayout'
import Authen from './Authen';

const Router = (props) => {
    return (
        <>
            <Routes>
                <Route path='/login' element={<Login/>}/>
                <Route path='*' element={<Authen><MasterLayout/></Authen>}/>
            </Routes>
        </>
    );
}
export default Router;