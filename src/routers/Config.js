import React from "react";

import { routes as dec } from "../views/Declaration";
const DashBoard = React.lazy(() => import('../Components/DashBoard'));
const PageNotFound = React.lazy(() => import('../Components/PageNotFound'));

const configRoute = [
    {
        path: '/',
        component: DashBoard,
        layout: 'Nguin'
    },
    ...dec,
    {
        path: '*',
        component: PageNotFound,
        layout: 'pagenotfound'
    },
]

export default configRoute;